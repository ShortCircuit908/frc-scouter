package com.orf4450.scouter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.*;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.orf4450.scouter.ui.RemoteDeviceWrapper;
import com.orf4450.scouter.ui.UploadCallback;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class UploadActivity extends Activity {
	private SwipeRefreshLayout list_devices_layout;
	private ListView list_devices;
	private BluetoothManager bluetooth_manager;
	private ArrayAdapter<RemoteDeviceWrapper> list_adapter;
	private ScouterDatabase database_helper;
	private Dialog status_dialog;
	private final BroadcastReceiver bluetooth_discovery_receiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			switch (action) {
				case BluetoothDevice.ACTION_FOUND:
					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					RemoteDeviceWrapper wrapper = new RemoteDeviceWrapper(device);
					for (int i = 0; i < list_adapter.getCount(); i++) {
						if (list_adapter.getItem(i).equals(wrapper)) {
							return;
						}
					}
					list_adapter.add(wrapper);
					list_adapter.notifyDataSetChanged();
					break;
				case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
					list_devices_layout.setRefreshing(true);
					break;
				case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
					list_devices_layout.setRefreshing(false);
					break;
			}
		}
	};

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.upload);

		list_devices_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		list_devices = (ListView) findViewById(R.id.list_devices);
		list_adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
		list_devices.setAdapter(list_adapter);

		bluetooth_manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		registerReceiver(bluetooth_discovery_receiver, filter);

		database_helper = new ScouterDatabase(this);

		bluetooth_manager.getAdapter().startDiscovery();
		attachListeners();
	}

	private void attachListeners() {
		list_devices_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				if (!bluetooth_manager.getAdapter().isDiscovering()) {
					bluetooth_manager.getAdapter().startDiscovery();
				}
			}
		});
		list_devices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				list_devices.setEnabled(false);
				status_dialog = new Dialog(UploadActivity.this);
				status_dialog.setContentView(R.layout.upload_status);
				status_dialog.show();
				if (bluetooth_manager.getAdapter().isDiscovering()) {
					bluetooth_manager.getAdapter().cancelDiscovery();
				}
				BluetoothDevice device = list_adapter.getItem(position).getDevice();
				new UploadTask(UploadActivity.this, database_helper, device, new UploadCallback() {
					@Override
					public void onUploadFinished(final Throwable e) {
						if (e != null) {
							UploadActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									new AlertDialog.Builder(UploadActivity.this)
											.setTitle("Upload failed")
											.setMessage(e.getClass() + ": " + e.getMessage())
											.setIcon(android.R.drawable.ic_dialog_alert)
											.setOnDismissListener(new DialogInterface.OnDismissListener() {
												@Override
												public void onDismiss(DialogInterface dialog) {
													UploadActivity.this.finish();
												}
											})
											.show();
								}
							});
						}
						else {
							UploadActivity.this.finish();
						}
					}
				});
			}
		});
		findViewById(R.id.button_reset_uploaded).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				database_helper.resetUploaded();
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (status_dialog != null) {
			status_dialog.dismiss();
		}
		unregisterReceiver(bluetooth_discovery_receiver);
	}
}
