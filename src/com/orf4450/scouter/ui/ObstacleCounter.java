package com.orf4450.scouter.ui;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.orf4450.scouter.ui.VerticalTextView;

import java.util.HashMap;

/**
 * @author ShortCircuit908
 *         Created on 1/12/2016
 */
public class ObstacleCounter {
	private final String db_column_name;
	private final NumberPicker picker_crossed;
	private final NumberPicker picker_failed;

	public ObstacleCounter(Context context, String name, String db_column_name, LinearLayout layout) {
		this.db_column_name = db_column_name;
		// The layout which contains all elements related to context counter
		LinearLayout parent_layout = new LinearLayout(context);
		parent_layout.setOrientation(LinearLayout.VERTICAL);
		GradientDrawable background_gradient = new GradientDrawable();
		background_gradient.setColor(0x0);
		background_gradient.setCornerRadius(5);
		background_gradient.setStroke(2, 0xFFEEEEEE);
		parent_layout.setBackground(background_gradient);
		TextView label = new TextView(context);
		label.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		label.setText(name);
		label.setGravity(Gravity.CENTER_HORIZONTAL);
		parent_layout.addView(label);
		// The layout which contains the individual number pickers and their labels
		LinearLayout picker_layout = new LinearLayout(context);
		picker_layout.setOrientation(LinearLayout.HORIZONTAL);
		// Components to count successful crossings
		label = new VerticalTextView(context);
		label.setText("Crossed");
		picker_layout.addView(label);
		picker_crossed = new NumberPicker(context);
		picker_crossed.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		picker_crossed.setMinValue(0);
		picker_crossed.setMaxValue(99);
		picker_layout.addView(picker_crossed);
		// Components to count failed crossings
		label = new VerticalTextView(context);
		label.setText("Failed");
		picker_layout.addView(label);
		picker_failed = new NumberPicker(context);
		picker_failed.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		picker_failed.setMinValue(0);
		picker_failed.setMaxValue(99);
		picker_layout.addView(picker_failed);
		// Add components
		parent_layout.addView(picker_layout);
		layout.addView(parent_layout);
	}

	public void saveState(Bundle state) {
		state.putInt(db_column_name + "_crossed", getCrossed());
		state.putInt(db_column_name + "_failed", getFailed());
	}

	public void loadState(Bundle state) {
		if (state != null) {
			if (state.containsKey(db_column_name + "_crossed")) {
				picker_crossed.setValue(state.getInt(db_column_name + "_crossed"));
			}
			if (state.containsKey(db_column_name + "_failed")) {
				picker_failed.setValue(state.getInt(db_column_name + "_failed"));
			}
		}
	}

	public void saveState(HashMap<String, Object> state) {
		state.put(db_column_name + "_crossed", getCrossed());
		state.put(db_column_name + "_failed", getFailed());
	}

	public void loadState(HashMap<String, Object> state) {
		if (state != null) {
			if (state.containsKey(db_column_name + "_crossed")) {
				picker_crossed.setValue((int) state.get(db_column_name + "_crossed"));
			}
			if (state.containsKey(db_column_name + "_failed")) {
				picker_failed.setValue((int) state.get(db_column_name + "_failed"));
			}
		}
	}

	public int getFailed() {
		return picker_failed.getValue();
	}

	public void setFailed(int failed){
		picker_failed.setValue(failed);
	}

	public int getCrossed() {
		return picker_crossed.getValue();
	}

	public void setCrossed(int crossed){
		picker_crossed.setValue(crossed);
	}

	public String getDbColumnName() {
		return db_column_name;
	}
}
