package com.orf4450.scouter.ui;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public interface UploadCallback {
	void onUploadFinished(Throwable e);
}
