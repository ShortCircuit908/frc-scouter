package com.orf4450.scouter.ui;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.orf4450.scouter.ui.VerticalTextView;

import java.util.HashMap;

/**
 * @author ShortCircuit908
 *         Created on 1/12/2016
 */
public class GoalCounter {
	private final CheckBox check_box_capable;
	private final NumberPicker picker_scored;
	private final NumberPicker picker_missed;
	private final String db_column_name;

	public GoalCounter(Context context, String name, String db_column_name, LinearLayout layout_goals) {
		this.db_column_name = db_column_name;
		// The layout which contains all elements related to context counter
		LinearLayout parent_layout = new LinearLayout(context);
		parent_layout.setOrientation(LinearLayout.VERTICAL);
		GradientDrawable background_gradient = new GradientDrawable();
		background_gradient.setColor(0x0);
		background_gradient.setCornerRadius(5);
		background_gradient.setStroke(2, 0xFFEEEEEE);
		parent_layout.setBackground(background_gradient);
		TextView label = new TextView(context);
		label.setTextAppearance(context, android.R.style.TextAppearance_Medium);
		label.setText(name);
		label.setGravity(Gravity.CENTER_HORIZONTAL);
		parent_layout.addView(label);
		check_box_capable = new CheckBox(context);
		check_box_capable.setText("Capable");
		parent_layout.addView(check_box_capable);
		// The layout which contains the individual number pickers and their labels
		LinearLayout picker_layout = new LinearLayout(context);
		picker_layout.setOrientation(LinearLayout.HORIZONTAL);
		// Components to count successful crossings
		label = new VerticalTextView(context);
		label.setText("Scored");
		picker_layout.addView(label);
		picker_scored = new NumberPicker(context);
		picker_scored.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		picker_scored.setMinValue(0);
		picker_scored.setMaxValue(99);
		picker_layout.addView(picker_scored);
		// Components to count failed crossings
		label = new VerticalTextView(context);
		label.setText("Missed");
		picker_layout.addView(label);
		picker_missed = new NumberPicker(context);
		picker_missed.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
		picker_missed.setMinValue(0);
		picker_missed.setMaxValue(99);
		picker_layout.addView(picker_missed);
		// Add components
		parent_layout.addView(picker_layout);
		layout_goals.addView(parent_layout);
	}

	public void saveState(Bundle state){
		state.putInt(db_column_name + "_capable", isCapable() ? 1 : 0);
		state.putInt(db_column_name + "_scored", getScored());
		state.putInt(db_column_name + "_missed", getMissed());
	}

	public void loadState(Bundle state){
		if(state != null){
			if(state.containsKey(db_column_name + "_capable")){
				check_box_capable.setChecked(state.getInt(db_column_name + "_capable") != 0);
			}
			if(state.containsKey(db_column_name + "_scored")){
				picker_scored.setValue(state.getInt(db_column_name + "_scored"));
			}
			if(state.containsKey(db_column_name + "_missed")){
				picker_missed.setValue(state.getInt(db_column_name + "_missed"));
			}
		}
	}

	public void saveState(HashMap<String, Object> state) {
		state.put(db_column_name + "_capable", isCapable() ? 1 : 0);
		state.put(db_column_name + "_scored", getScored());
		state.put(db_column_name + "_missed", getMissed());
	}

	public void loadState(HashMap<String, Object> state) {
		if (state != null) {
			if (state.containsKey(db_column_name + "_capable")) {
				check_box_capable.setChecked(state.get(db_column_name + "_capable") != 0);
			}
			if (state.containsKey(db_column_name + "_scored")) {
				picker_scored.setValue((int) state.get(db_column_name + "_scored"));
			}
			if (state.containsKey(db_column_name + "_missed")) {
				picker_missed.setValue((int) state.get(db_column_name + "_missed"));
			}
		}
	}

	public int getMissed() {
		return picker_missed.getValue();
	}

	public void setMissed(int missed){
		picker_missed.setValue(missed);
	}

	public int getScored() {
		return picker_scored.getValue();
	}

	public void setScored(int scored){
		picker_scored.setValue(scored);
	}

	public boolean isCapable() {
		return check_box_capable.isChecked();
	}

	public void setCapable(boolean capable){
		check_box_capable.setChecked(capable);
	}

	public String getDbColumnName() {
		return db_column_name;
	}
}
