package com.orf4450.scouter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.*;
import android.widget.*;
import com.orf4450.scouter.ui.GoalCounter;
import com.orf4450.scouter.ui.ObstacleCounter;
import com.orf4450.scouter.ui.TimedConfirmation;

import java.util.HashMap;
import java.util.LinkedList;

public class ScoutActivity extends Activity {
	public static final int ALLIANCE_COLOR_RED = 0xFFB20B00;
	public static final int ALLIANCE_COLOR_BLUE = 0xFF001AAF;
	private int current_alliance_color = ALLIANCE_COLOR_RED;
	private final LinkedList<ObstacleCounter> obstacle_counters = new LinkedList<>();
	private final LinkedList<GoalCounter> goal_counters = new LinkedList<>();
	private LinearLayout layout_obstacles;
	private LinearLayout layout_goals;
	private ToggleButton toggle_mode;
	private ToggleButton toggle_alliance;
	private TextView text_match;
	private TextView text_team;
	private Button button_save;
	private Button button_reset;
	private CheckBox check_box_climb_capable;
	private CheckBox check_box_climb_successful;
	private final ScouterDatabase database_helper;
	private HashMap<String, HashMap<String, Object>> current_data_bundle = new HashMap<>(2);

	public ScoutActivity() {
		super();
		database_helper = new ScouterDatabase(this);
	}

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		setContentView(R.layout.main);

		// Populate view fields
		layout_obstacles = (LinearLayout) findViewById(R.id.layout_obstacles);
		layout_goals = (LinearLayout) findViewById(R.id.layout_goals);
		toggle_alliance = (ToggleButton) findViewById(R.id.toggle_alliance);
		toggle_mode = (ToggleButton) findViewById(R.id.toggle_phase);
		text_match = (TextView) findViewById(R.id.text_match);
		text_team = (TextView) findViewById(R.id.text_team);
		button_save = (Button) findViewById(R.id.button_save);
		button_reset = (Button) findViewById(R.id.button_reset);
		check_box_climb_capable = (CheckBox) findViewById(R.id.check_box_climb_capable);
		check_box_climb_successful = (CheckBox) findViewById(R.id.check_box_climb_successful);

		// Generate obstacle components
		addObstacleCounter("Moat", "moat");
		addObstacleCounter("Portcullis", "portcullis");
		addObstacleCounter("Sally Port", "sallyport");
		addObstacleCounter("Ramparts", "ramparts");
		addObstacleCounter("Chival de Frise", "chivaldefrise");
		addObstacleCounter("Rock Wall", "rockwall");
		addObstacleCounter("Rough Terrain", "roughterrain");
		addObstacleCounter("Low Bar", "lowbar");

		// Generate goal components
		addGoalCounter("Low Goal Left", "lowgoalleft");
		addGoalCounter("Low Goal Right", "lowgoalright");
		addGoalCounter("High Goal Left", "highgoalleft");
		addGoalCounter("High Goal Center", "highgoalcenter");
		addGoalCounter("High Goal Right", "highgoalright");

		// Add listeners
		attachListeners();

		// We might have stored the current color for orientation changes
		if (state != null) {
			if (state.containsKey("current_alliance_color")) {
				current_alliance_color = state.getInt("current_alliance_color");
			}
		}

		// Initialize all components to default values
		resetAll();
	}

	private void attachListeners() {
		// Toggle alliance between blue and red
		toggle_alliance.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setAllianceColor(((ToggleButton) v).isChecked() ? ALLIANCE_COLOR_BLUE : ALLIANCE_COLOR_RED);
			}
		});
		// Toggle mode between autonomous and teleoperated
		toggle_mode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Save the current component states
				saveToBundle(!toggle_mode.isChecked());
				String match = text_match.getText().toString();
				String team = text_team.getText().toString();
				// Load next component state
				loadFromBundle(toggle_mode.isChecked());
				text_match.setText(match);
				text_team.setText(team);
			}
		});
		// TextWatcher to ensure non-empty fields
		TextWatcher empty_field_watcher = new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				boolean enabled = text_match.length() > 0 && text_team.length() > 0;
				toggle_mode.setEnabled(enabled);
				button_save.setEnabled(enabled);
			}
		};
		text_match.addTextChangedListener(empty_field_watcher);
		text_team.addTextChangedListener(empty_field_watcher);
		// Save button
		button_save.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				createToast("Saving...");
				// Save current state
				saveToBundle(toggle_mode.isChecked());
				// Commit all states to the database
				database_helper.saveMatch(current_data_bundle);
				createToast("Match saved");
				// Reset all components to default values
				resetAll();
			}
		});
		// Reset button
		button_reset.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Reset all fields
				resetAll();
				createToast("Match data reset");
			}
		});
		check_box_climb_capable.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (check_box_climb_capable.isChecked()) {
					check_box_climb_successful.setEnabled(true);
				}
				else {
					check_box_climb_successful.setChecked(false);
					check_box_climb_successful.setEnabled(false);
				}
			}
		});
	}

	public void resetAll() {
		// Reset current state and load
		current_data_bundle.clear();
		loadFromBundle(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		// Load matches
		menu.findItem(R.id.load).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// Create a popup window
				final Dialog dialog = new Dialog(ScoutActivity.this);
				dialog.setContentView(R.layout.load_match);
				ListView list_view = (ListView) dialog.findViewById(R.id.list_load_match);
				// Create an ArrayAdapter and populate it with stored match descriptors
				final ArrayAdapter<MatchDescriptor> adapter = new ArrayAdapter<>(ScoutActivity.this,
						android.R.layout.simple_list_item_1);
				list_view.setAdapter(adapter);
				adapter.addAll(database_helper.getAllStoredMatches());
				adapter.notifyDataSetChanged();
				// Load matches on click
				list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						MatchDescriptor descriptor = adapter.getItem(position);
						createToast("Loading...");
						ScoutActivity.this.current_data_bundle = database_helper.loadMatch(
								descriptor.getMatchNumber(), descriptor.getTeamNumber());
						loadFromBundle(false);
						dialog.dismiss();
					}
				});
				list_view.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
						PopupMenu menu = new PopupMenu(ScoutActivity.this, view);
						MenuItem item = menu.getMenu().add("Remove");
						item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
							@Override
							public boolean onMenuItemClick(MenuItem item) {
								MatchDescriptor descriptor = adapter.getItem(position);
								createToast("Deleting...");
								database_helper.deleteMatch(descriptor.getMatchNumber(), descriptor.getTeamNumber());
								createToast("Match deleted");
								adapter.remove(descriptor);
								adapter.notifyDataSetChanged();
								return true;
							}
						});
						menu.show();
						return false;
					}
				});
				// Show the dialog
				dialog.show();
				return true;
			}
		});
		// Delete all data
		menu.findItem(R.id.delete_all).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				final Dialog dialog = new Dialog(ScoutActivity.this);
				dialog.setContentView(R.layout.delete_all);
				final ProgressBar progress_bar = (ProgressBar) dialog.findViewById(R.id.progress_delete_all);
				// Create a progress bar-based timed confirmation system
				new TimedConfirmation(3000, progress_bar, new Runnable() {
					@Override
					public void run() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								// Delete all data
								createToast("Deleting...");
								database_helper.deleteAllData();
								resetAll();
								createToast("All data deleted");
								dialog.dismiss();
							}
						});
					}
				});
				dialog.show();
				return false;
			}
		});
		// Upload data
		menu.findItem(R.id.upload).setOnMenuItemClickListener(
				new MenuItem.OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						startActivity(new Intent(ScoutActivity.this, UploadActivity.class));
						return true;
					}
				}
		);
		return true;
	}

	public void saveToBundle(boolean teleop) {
		// Create a new Bundle big enough to store component states
		HashMap<String, Object> bundle = new HashMap<>(4 + (goal_counters.size() * 3) + (obstacle_counters.size() * 4));
		bundle.put("match_number", Integer.parseInt(text_match.getText().toString()));
		bundle.put("team_number", Integer.parseInt(text_team.getText().toString()));
		bundle.put("alliance", toggle_alliance.isChecked() ? 1 : 0);
		bundle.put("teleop", teleop ? 1 : 0);
		bundle.put("climb_capable", check_box_climb_capable.isChecked() ? 1 : 0);
		bundle.put("climb_successful", check_box_climb_successful.isChecked() ? 1 : 0);
		// Store obstacle component states
		for (ObstacleCounter counter : obstacle_counters) {
			counter.saveState(bundle);
		}
		// Store goal component states
		for (GoalCounter counter : goal_counters) {
			counter.saveState(bundle);
		}
		// Store local bundle to global state
		current_data_bundle.put(teleop ? "teleop" : "autonomous", bundle);
	}

	public HashMap<String, HashMap<String, Object>> getCurrentDataBundle() {
		return current_data_bundle;
	}

	public void loadFromBundle(boolean teleop) {
		HashMap<String, Object> bundle = current_data_bundle.get(teleop ? "teleop" : "autonomous");
		if (bundle == null || bundle.size() < 4) {
			bundle = new HashMap<>(4 + (goal_counters.size() * 3) + (obstacle_counters.size() * 4));
			bundle.put("match_number", database_helper.getLastMatchNumber() + 1);
			bundle.put("team_number", -1);
			bundle.put("alliance", 0);
			bundle.put("teleop", teleop ? 1 : 0);
			for (ObstacleCounter counter : obstacle_counters) {
				counter.setCrossed(0);
				counter.setFailed(0);
				counter.saveState(bundle);
			}
			for (GoalCounter counter : goal_counters) {
				counter.setCapable(false);
				counter.setMissed(0);
				counter.setScored(0);
				counter.saveState(bundle);
			}
			bundle.put("climb_capable", 0);
			bundle.put("climb_successful", 0);
			current_data_bundle.put(teleop ? "teleop" : "autonomous", bundle);
		}
		text_match.setText(bundle.get("match_number") + "");
		text_team.setText((int) bundle.get("team_number") < 0 ? "" : bundle.get("team_number") + "");
		toggle_alliance.setChecked(bundle.get("alliance") != 0);
		setAllianceColor(toggle_alliance.isChecked() ? ALLIANCE_COLOR_BLUE : ALLIANCE_COLOR_RED);
		toggle_mode.setChecked(bundle.get("teleop") != 0);
		boolean enabled = text_match.length() > 0 && text_team.length() > 0;
		toggle_mode.setEnabled(enabled);
		for (ObstacleCounter counter : obstacle_counters) {
			counter.loadState(bundle);
		}
		for (GoalCounter counter : goal_counters) {
			counter.loadState(bundle);
		}
		check_box_climb_capable.setChecked(bundle.get("climb_capable") != 0);
		check_box_climb_successful.setEnabled(check_box_climb_capable.isChecked());
		check_box_climb_successful.setChecked(bundle.get("climb_successful") != 0);
	}

	@Override
	public void onSaveInstanceState(Bundle state) {
		super.onSaveInstanceState(state);
		state.putInt("current_alliance_color", current_alliance_color);
		for (ObstacleCounter counter : obstacle_counters) {
			counter.saveState(state);
		}
		for (GoalCounter counter : goal_counters) {
			counter.saveState(state);
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle state) {
		super.onRestoreInstanceState(state);
		for (ObstacleCounter counter : obstacle_counters) {
			counter.loadState(state);
		}
		for (GoalCounter counter : goal_counters) {
			counter.loadState(state);
		}
	}


	public void setAllianceColor(int color) {
		View view = findViewById(R.id.background);
		view.setBackgroundColor(color);
		current_alliance_color = color;
	}

	private void addObstacleCounter(String name, String db_column_name) {
		obstacle_counters.add(new ObstacleCounter(this, name, db_column_name, layout_obstacles));
	}

	private void addGoalCounter(String name, String db_column_name) {
		goal_counters.add(new GoalCounter(this, name, db_column_name, layout_goals));
	}

	public void createToast(String text) {
		Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.show();
	}
}
