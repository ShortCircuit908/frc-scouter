package com.orf4450.scouter;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import com.orf4450.scouter.ui.UploadCallback;

import java.util.UUID;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class UploadTask implements Runnable {
	private final Activity context;
	private final ScouterDatabase database_helper;
	private final BluetoothDevice device;
	private final UploadCallback callback;

	public UploadTask(Activity context, ScouterDatabase database_helper, BluetoothDevice device, UploadCallback callback) {
		this.context = context;
		this.database_helper = database_helper;
		this.device = device;
		this.callback = callback;
		new Thread(this).start();
	}

	@Override
	public void run() {
		Throwable thrown = null;
		try {
			BluetoothSocket socket = device.createRfcommSocketToServiceRecord(
					UUID.fromString("7674047e-6e47-4bf0-831f-209e3f9dd23f"));
			socket.connect();
			database_helper.upload(socket.getOutputStream());
			socket.close();
		}
		catch (Throwable e) {
			thrown = e;
		}
		System.out.println("thrown = " + thrown);
		callback.onUploadFinished(thrown);
	}
}
